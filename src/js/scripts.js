jQuery(document).ready(function($) {

  if($(window).width() > 768) {
    $('#hero').css('background-image','url('+$('#hero').data('img-lg')+')');
  } else {
    $('#hero').css('background-image','url('+$('#hero').data('img-sm')+')');
  }

  $(window).resize(function(){
    if($(window).width() > 768) {
      $('#hero').css('background-image','url('+$('#hero').data('img-lg')+')');
    } else {
      $('#hero').css('background-image','url('+$('#hero').data('img-sm')+')');
    }    
  });

  new WOW().init();

  var rellax = new Rellax('.rellax');

  var rootClassName = 'owl-container';
  var navContainerClassName = '.navigation-arrows';
  var dotsContainerClassName = '.navigation-dots';
  $('.owl-gallery').each( function (index, item) {
    var items = $(this).data('items');
    var stagePadding = $(this).data('padding');
    if( !items ) { items = 1; }
    if( !stagePadding ) { stagePadding = 0; }
    if( stagePadding ) { stagePaddingXs = 10; } else { stagePaddingXs = 0; }
    var rootIdClassName = 'owl-' + rootClassName + '-' + index;
    $(item).closest('.' + rootClassName).addClass(rootIdClassName);
    $(item).on('initialized.owl.carousel', function(event){ 
      $('.container-fluid.relative').each(function(){
        $(this).css('min-height',$(this).find('.left-half').height());
        $(this).css('min-height',$(this).find('.right-half').height());
      });
    })
    $(item).owlCarousel({
      lazyLoad:true,
      nav:true,
      smartSpeed:1000,
      navContainer: '.' + rootIdClassName + ' ' +navContainerClassName,
      dotsContainer: '.' + rootIdClassName + ' ' +dotsContainerClassName,
      responsive : {
        0 : {
          margin:25,
          items:1,
          stagePadding:stagePaddingXs
        },
        768 : {
          margin:35,
          items:items,
          stagePadding:stagePadding
        }
      }
    });
  });

  $('.menu-toggle').click(function(){
    $('.menu-toggle').toggleClass('open');
    $('.menu-mobile').toggleClass('open');
  });

  $('.menu-mobile a').click(function(){
    $('.menu-toggle').removeClass('open');
    $('.menu-mobile').removeClass('open');
  });

  // $('input').change(function(){
  //   if ($(this).val() != '') { 
  //     $(this).removeClass('empty');
  //     $('input[type="submit"]').removeAttr('disabled');
  //     $('input[type="submit"]').removeClass('disabled');
  //   } else {
  //     $(this).addClass('empty');
  //   }
  //   $('.empty').each(function(){
  //     $('input[type="submit"]').attr('disabled','disabled');
  //     $('input[type="submit"]').addClass('disabled');
  //   });
  // });

  $('body:not(.home) #masthead li a, body:not(.home) .menu-mobile a').each(function(){
    var link = $(this).attr("href").replace('#','../#');
    $(this).attr("href", link);
  });

  $("input[type='tel']")
  .mask("(99) 9999-9999?9")
  .focusout(function (event) {  
    var target, phone, element;  
    target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
    phone = target.value.replace(/\D/g, '');
    element = $(target);  
    element.unmask();  
    if(phone.length > 10) {  
      element.mask("(99) 99999-999?9");  
    } else {  
      element.mask("(99) 9999-9999?9");  
    }  
  });

  // var hero = $('#hero').height();

  // $(window).scroll(function(){
  //   if($(this).scrollTop()<(hero-300)){
  //     $('#hero').css('top',($(this).scrollTop() * -1));
  //     $('#empreendimento').css('padding-top',($(this).scrollTop() + 100));
  //   }
  // });

  $(window).scroll( function( e ){ 
    if( $(this).scrollTop() > $('.white-pink').offset().top ){
      if( $(document).scrollTop() > ($('.white-pink').offset().top + $('.white-pink').outerHeight()) ){
        $(".menu-toggle").removeClass("white");
      } else {
        $(".menu-toggle").addClass("white");
      }
    } else {
      $(".menu-toggle").removeClass("white");
    }

  });

  if ( $('#gmaps').length ) {
    var lat = $('#gmaps').data('lat');
    var lng = $('#gmaps').data('lng');
    var endereco = {lat: lat, lng: lng};

    var map = new google.maps.Map(
      document.getElementById('gmaps'), {
        zoom: 15,
        center: endereco,
        streetViewControl : false,
        mapTypeControl: false,
        styles: [
        {
          "featureType": "water",
          "elementType": "geometry.fill",
          "stylers": [
          {
            "color": "#75cff0"
          }
          ]
        }
        ]
      });

    var infowindow =  new google.maps.InfoWindow();
    var marker = new google.maps.Marker({position: endereco, map: map, icon: stylesheet_directory_uri + '/images/pin.png'});
    bindInfoWindow(marker, map, infowindow, "Janeiro");

    function bindInfoWindow(marker, map, infowindow, name) {
      marker.addListener('click', function() {
        infowindow.setContent(name);
        infowindow.open(map, this);
      });
    }

  }

});
