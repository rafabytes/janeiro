<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordpressGulpBoilerplate
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.png">

	<script type="text/javascript">
		var stylesheet_directory_uri = "<?php echo get_stylesheet_directory_uri(); ?>";
	</script>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<div class="menu-mobile">
		<img class="logo" src="<?php echo get_template_directory_uri() ?>/images/logo-alt.svg" alt="Janeiro">
		<div class="menu-toggle d-md-none"></div>
		<ul>
			<li><a href="#empreendimento">O Empreendimento</a></li>
			<li><a href="#diferenciais">Diferenciais</a></li>
			<li><a href="#projeto">O Projeto</a></li>
			<li><a href="#ficha">Ficha Técnica</a></li>
			<li><a href="#plantas">Plantas</a></li>
			<li><a href="#ebook">Andamento da obra</a></li>
			<li><a href="#mapa">Localização</a></li>
			<li><a href="#contato">Contato</a></li>
		</ul>
		<ul class="social">
			<li>
				<a href="#" class="icon instagram"></a>
			</li>
			<li>
				<a href="#" class="icon facebook"></a>
			</li>
		</ul>
		<img class="figure" src="<?php echo get_template_directory_uri() ?>/images/parallax-06.svg">
	</div>

	<div class="menu-toggle d-md-none"></div>

	<div id="page" class="site">

		<header id="masthead" class="site-header animated fadeInDown">
			<div class="container">
				<div class="site-branding">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_template_directory_uri() ?>/images/logo.svg" alt="Janeiro"></a>
				</div><!-- .site-branding -->

				<ul class="main-menu d-none d-md-block">
					<li><a href="#empreendimento">O Empreendimento</a></li>
					<li><a href="#projeto">O Projeto</a></li>
					<li><a href="#ebook">Andamento da obra</a></li>
					<li><a href="#contato">A Urban</a></li>
				</ul>

				<ul class="menu-right d-none d-md-block">
					<li><a href="#mapa">Localização</a></li>
					<li><a href="#contato">Contato</a></li>
				</ul>


				<!-- <nav id="site-navigation" class="main-navigation">
					<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
					) );
					?>
				</nav> #site-navigation -->
			</div>
		</header><!-- #masthead -->

		<div id="content" class="site-content">
