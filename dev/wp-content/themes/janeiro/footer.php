<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordpressGulpBoilerplate
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="site-info">
			<div class="container">
				<div class="row">
					<div class="col-md-2">
						<img class="logo" src="<?php echo get_template_directory_uri() ?>/images/logo-urban.svg">
					</div>
					<div class="col-md-3">
						<p>O Urban Janeiro está localizado na Av. Arthur Monteiro de Paiva, esquina com Av. Presidente Café Filho, na orla do Bessa.</p>
						<div class="d-md-none"><h2>Vendas<br>83 99328-5014<br><a href="https://vivaurban.com.br">vivaurban.com.br</a></h2></div>
						<p class="pdp"><a href="politica-de-privacidade">Política de Privacidade</a></p>
						<p class="copyrights">Todos os direitos reservados. Urban © 2020</p>
					</div>
					<div class="col-md-6 offset-md-1">
						<div class="d-none d-md-block"><h2>Vendas<br>83 99328-5014<br><a href="https://vivaurban.com.br">vivaurban.com.br</a></h2></div>
						<a href="#" class="qualitare"></a>
					</div>
				</div>
				<img class="bottom" src="<?php echo get_template_directory_uri() ?>/images/parallax-12.svg">
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>

</body>
</html>
