<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordpressGulpBoilerplate
 */

/* Template name: Home */
get_header();
?>
<div id="primary" class="content-area">
	<main id="main" class="site-main">

		<section id="hero" data-img-sm="<?php the_field('imagem_1_sm'); ?>" data-img-lg="<?php the_field('imagem_1_lg'); ?>">

			<div class="container hero-content">
				<h5 class="animated fadeInRight"><?php the_field('titulo_1'); ?></h5>
				<h1 class="animated fadeInRight"><?php the_field('cabecalho_1'); ?></h1>
				<p class="animated fadeInRight"><?php the_field('subtitulo_1'); ?></p>
				<a href="<?php the_field('link_1'); ?>" class="btn animated fadeInUp"><?php the_field('botao_1'); ?></a>
				<div class="scroll d-md-none animated fadeIn">Role para baixo</div>
				<div class="parallax-01 rellax animated fadeIn" data-rellax-speed="-4"><img src="<?php echo get_template_directory_uri() ?>/images/parallax-01.svg"></div>
			</div>

			<div class="parallax-02 rellax animated fadeIn d-md-none" data-rellax-speed="2"><img src="<?php echo get_template_directory_uri() ?>/images/parallax-02.svg"></div>

			<div class="parallax-03 rellax animated fadeIn d-md-none" data-rellax-speed="3"><img src="<?php echo get_template_directory_uri() ?>/images/parallax-03.svg"></div>

			<div class="parallax-04 rellax animated fadeIn d-md-none" data-rellax-speed="6"><img src="<?php echo get_template_directory_uri() ?>/images/parallax-04.svg"></div>

			<div class="hero-cta animated fadeInUp d-none d-md-block">
				<div class="parallax-02 rellax animated fadeIn" data-rellax-speed="-2"><img src="<?php echo get_template_directory_uri() ?>/images/parallax-02.svg"></div>
				<div class="container white">
					<div class="row">
						<div class="col-md-4">
							<a href="<?php the_field('link_2'); ?>">
								<h5 class="tag"><?php the_field('titulo_2'); ?></h5>
								<h3><?php the_field('cabecalho_2'); ?></h3>
								<p class="sub"><?php the_field('subtitulo_2'); ?></p>
							</a>
						</div>
						<div class="col-md-4">
							<a href="<?php the_field('link_3'); ?>">
								<h5 class="tag"><?php the_field('titulo_3'); ?></h5>
								<h3><?php the_field('cabecalho_3'); ?></h3>
								<p class="sub"><?php the_field('subtitulo_3'); ?></p>
							</a>
						</div>
						<div class="col-md-4">
							<a href="<?php the_field('link_4'); ?>">
								<h5 class="tag"><?php the_field('titulo_4'); ?></h5>
								<h3><?php the_field('cabecalho_4'); ?></h3>
								<p class="sub"><?php the_field('subtitulo_4'); ?></p>
							</a>
						</div>
					</div>
					<div class="parallax-03 rellax animated fadeIn" data-rellax-speed="3"><img src="<?php echo get_template_directory_uri() ?>/images/parallax-03.svg"></div>
					<div class="parallax-04 rellax animated fadeIn" data-rellax-speed="6"><img src="<?php echo get_template_directory_uri() ?>/images/parallax-04.svg"></div>
				</div>
			</div>

		</section>

		<section id="empreendimento">

			<div class="container-fluid relative">
				<!-- <div class="row"> -->
					<div class="container">
						<div class="row">
							<div class="col-md-6 left-half">
								<div class="row-fluid">
									<img class="featured wow fadeInLeft" src="<?php the_field('imagem_5') ?>">
									<div class="parallax-05 rellax animated fadeIn" data-rellax-speed="1"><img src="<?php echo get_template_directory_uri() ?>/images/parallax-05.svg"></div>
								</div>
							</div>
							<div class="col-md-5 offset-md-7">
								<h5 class="wow fadeInUp"><?php the_field('titulo_5'); ?></h5>
								<h1 class="wow fadeInUp"><?php the_field('cabecalho_5'); ?></h1>
								<div class="wow fadeInUp"><?php the_field('texto_5'); ?></div>
							</div>
							<div class="col-md-6">
								<h1 class="uppercase wow fadeInRight d-none d-md-block"><?php the_field('titulo_galeria_5'); ?></h1>
							</div>
						</div>
					</div>
					<!-- </div> -->
				</div>

				<div class="parallax-container">
					<div class="container">
						<?php 
						$images = acf_photo_gallery( 'galeria_5' , get_the_ID() );
						if ( is_array($images) || is_object($images) ) : ?>
							<div class="owl-container wow fadeInUp">
								<div id="owlEmpreendimento" class="owl-gallery owl-carousel" data-items="1" data-padding="0">
									<?php foreach( $images as $image ): ?>
										<div class="item">
											<a href="<?php echo $image['full_image_url'] ?>" data-fancybox="owlEmpreendimento" data-caption="<?php echo $image['caption'] ?>">
												<div class="img" style="background-image: url(<?php echo $image['full_image_url']; ?>);"></div>
											</a>
										</div>
									<?php endforeach; ?>
								</div>
								<div class="navigation-dots d-none d-md-flex"></div>
								<div class="navigation-arrows"></div>
							</div>
						<?php endif; ?>
					</div>
					<div class="parallax-06 rellax animated fadeIn" data-rellax-speed="1"><img src="<?php echo get_template_directory_uri() ?>/images/parallax-06.svg"></div>
				</div>

				<div class="container">
					<div class="row no-gutters">
						<div class="col-md-6">
							<h1 class="uppercase wow fadeInRight d-md-none"><?php the_field('titulo_galeria_5'); ?></h1>
						</div>
					</div>
				</div>

			</section>

			<section id="descubra">
				<div class="parallax-02 rellax animated fadeIn d-md-none" data-rellax-speed="2"><img src="<?php echo get_template_directory_uri() ?>/images/parallax-02.svg"></div>
				<div class="container">
					<div class="row">
						<div class="col">
							<h5 class="wow fadeInUp"><?php the_field('titulo_6'); ?></h5>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<h2 class="wow fadeInUp"><?php the_field('cabecalho_6'); ?></h2>
						</div>
						<div class="col-md-6">
							<div class="wow fadeInUp"><?php the_field('texto_6'); ?></div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<?php 
							$images = acf_photo_gallery( 'galeria_6' , get_the_ID() );
							if ( is_array($images) || is_object($images) ) : ?>
								<div class="owl-container">
									<div id="owlDescubra" class="owl-gallery owl-carousel overflow wow fadeInUp" data-items="1" data-padding="100">
										<?php foreach( $images as $image ): ?>
											<div class="item">
												<a href="<?php echo $image['full_image_url'] ?>" data-fancybox="owlDescubra" data-caption="<?php echo $image['caption'] ?>">
													<div class="img" style="background-image: url(<?php echo $image['full_image_url']; ?>);"></div>
												</a>
											</div>
										<?php endforeach; ?>
									</div>
									<div class="navigation-dots"></div>
									<div class="navigation-arrows d-none d-md-flex"></div>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</section>

			<section id="diferenciais">
				<div class="container">
					<div class="parallax-07 rellax animated fadeIn" data-rellax-speed="1"><img src="<?php echo get_template_directory_uri() ?>/images/parallax-07.svg"></div>
					<div class="row">
						<div class="col-md-12">
							<h5 class="wow fadeInUp"><?php the_field('titulo_7'); ?></h5>
							<h2 class="wow fadeInUp"><?php the_field('cabecalho_7'); ?></h2>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<?php 
							$images = acf_photo_gallery( 'galeria_7' , get_the_ID() );
							if ( is_array($images) || is_object($images) ) : $i=1; ?>
							<div class="owl-container">
								<div id="owlDiferenciais" class="owl-gallery owl-carousel overflow wow fadeInUp" data-items="2">
									<?php foreach( $images as $image ): ?>
										<div class="item">
											<div class="item-img">
												<a href="<?php echo $image['full_image_url'] ?>" data-fancybox="owlDiferenciais" data-caption="<?php echo $image['caption'] ?>">
													<div class="img" style="background-image: url(<?php echo $image['full_image_url']; ?>);"></div>
												</a>
											</div>
											<p>0<?php echo $i ?>.</p>
											<p><?php echo esc_attr($image['caption']); ?></p>
										</div>
										<?php $i++; endforeach; ?>
									</div>
									<div class="navigation-dots"></div>
									<div class="navigation-arrows d-none d-md-flex"></div>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</section>

			<section id="projeto" style="background-image: url(<?php the_field('imagem_8'); ?>);">
				<a href="<?php the_field('video_8'); ?>" data-fancybox class="btn-play"></a>
				<div class="container-fluid half-white">
					<div class="container">
						<div class="row">
							<div class="col-md-7 white header">
								<h5 class="wow fadeInUp"><?php the_field('titulo_8'); ?></h5>
								<h2 class="wow fadeInUp"><?php the_field('cabecalho_8'); ?></h2>
							</div>
						</div>
					</div>
				</div>
				<div class="container-fluid white">
					<div class="container">
						<div class="row">
							<div class="col-md-7">
								<p class="wow fadeInUp"><?php the_field('texto_8'); ?></p>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section id="plantas">
				<div class="container-fluid relative">
					<div class="parallax-08 rellax animated fadeIn" data-rellax-speed="0.5"><img src="<?php echo get_template_directory_uri() ?>/images/parallax-08.svg"></div>
					<div class="row">
						<div class="container">
							<div class="row">
								<div class="col-md-6">
									<div class="plantas-details">
										<h5 class="wow fadeInUp"><?php the_field('titulo_9'); ?></h5>
										<h2 class="wow fadeInUp"><?php the_field('cabecalho_9'); ?></h2>
										<div class="wow fadeInUp"><?php the_field('texto_9'); ?></div>
									<!-- <h6>apartamentos</h6>
									<ul>						
										<li>- 92m2 a 241m²</li>
										<li>- 2 e 3 quartos</li>
										<li>- 2 ou 3 vagas de garagem</li>
									</ul>
									<h6>áreas comuns</h6>
									<ul>
										<li>- Piscina com borda infinita</li>
										<li>- Salão de festas</li>
										<li>- Academia</li>
										<li>- Brinquedoteca</li>
										<li>- Pátio Central</li>
										<li>- Restaurante Gourmet & Coworking</li>
										<li>- Bicicletário</li>
									</ul>
									<h6>Previsão de entrega</h6>
									<ul>
										<li>23/09/2023</li>
									</ul> -->
								</div>
							</div>
							<div class="col-md-6 right-half half-white-v"></div>
							<div class="col-md-6">
								<?php 
								$images = acf_photo_gallery( 'galeria_9' , get_the_ID() );
								if ( is_array($images) || is_object($images) ) : ?>
									<div class="owl-container half-white-v wow fadeInRight d-none d-md-block">
										<div id="owlPlantas" class="owl-gallery owl-carousel" data-items="1">
											<?php foreach( $images as $image ): ?>
												<div class="item">
													<div class="item-img">
														<a href="<?php echo $image['full_image_url'] ?>" data-fancybox="owlPlantas" data-caption="<?php echo $image['caption'] ?>">
															<div class="img" style="background-image: url(<?php echo $image['full_image_url']; ?>);"></div>
														</a>
													</div>
													<div class="item-sub">
														<h3><?php echo esc_attr($image['title']); ?></h3>
														<p><?php echo esc_attr($image['caption']); ?></p>
													</div>
												</div>
											<?php endforeach; ?>
										</div>
										<div class="navigation-dots"></div>
										<div class="navigation-arrows"></div>
									</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row-fluid">
					<?php 
					$images = acf_photo_gallery( 'galeria_9' , get_the_ID() );
					if ( is_array($images) || is_object($images) ) : ?>
						<div class="owl-container half-white-v wow fadeInUp d-md-none">
							<div id="owlPlantas" class="owl-gallery owl-carousel" data-items="1">
								<?php foreach( $images as $image ): ?>
									<div class="item">
										<div class="item-img">
											<div class="img" style="background-image: url(<?php echo $image['full_image_url']; ?>);"></div>
										</div>
										<div class="item-sub">
											<h3><?php echo esc_attr($image['title']); ?></h3>
											<p><?php echo esc_attr($image['caption']); ?></p>
										</div>
									</div>
								<?php endforeach; ?>
							</div>
							<div class="navigation-dots"></div>
							<div class="navigation-arrows"></div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</section>

		<section id="ebook">
			<div class="ebook-padding">
				<div class="parallax-09 rellax animated fadeIn" data-rellax-speed="1"><img src="<?php echo get_template_directory_uri() ?>/images/parallax-09.svg"></div>
				<div class="parallax-10 rellax animated fadeIn" data-rellax-speed="1"><img src="<?php echo get_template_directory_uri() ?>/images/parallax-10.svg"></div>
			</div>
			<div class="container-fuild blue-purple">
				<div class="row">
					<div class="container">
						<div class="row">
							<div class="col-md-5 ebook-cover">
								<img src="<?php echo get_template_directory_uri() ?>/images/ebook.svg" class="d-none d-md-block">
							</div>
							<div class="col-md-7 text-center purple">
								<h5 class="wow fadeInUp"><?php the_field('titulo_10'); ?></h5>
								<h2 class="wow fadeInUp"><?php the_field('cabecalho_10'); ?></h2>
								<img class="ebook-cover-xs d-md-none" src="<?php echo get_template_directory_uri() ?>/images/ebook-xs.png">
								<a href="<?php the_field('ebook_10'); ?>" class="btn secondary wow fadeIn" target="_blank"><?php the_field('botao_10'); ?></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section id="obra">
			<div class="container-fluid white-pink">
				<div class="parallax-11 rellax animated fadeIn" data-rellax-speed="1"><img src="<?php echo get_template_directory_uri() ?>/images/parallax-11.svg"></div>
				<div class="container">
					<div class="row">
						<div class="col-md-4 localizacao d-none d-md-block">
							<h5 class="wow fadeInUp"><?php the_field('titulo_11'); ?></h5>
							<h2 class="wow fadeInUp"><?php the_field('cabecalho_11'); ?></h2>
							<div class="wow fadeInUp"><?php the_field('texto_11'); ?></div>
						</div>
						<div class="col-md-8 andamento pink">

							<h2 class="wow fadeInUp"><?php the_field('titulo_12'); ?></h2>
							<p class="wow fadeInUp"><?php the_field('texto_12'); ?></p>

							<div class="row status-list">

								<div class="col-md-6">
									<div class="status wow fadeInRight">
										<h6>projeto</h6>
										<h4><?php the_field('projeto'); ?>%</h4>
										<div class="status-track">
											<div class="status-complete" style="width: <?php the_field('projeto'); ?>%"></div>
										</div>
									</div>
									<div class="status wow fadeInRight">
										<h6>estrutura</h6>
										<h4><?php the_field('estrutura'); ?>%</h4>
										<div class="status-track">
											<div class="status-complete" style="width: <?php the_field('estrutura'); ?>%"></div>
										</div>
									</div>
									<div class="status wow fadeInRight">
										<h6>revestimento externo</h6>
										<h4><?php the_field('revestimento_externo'); ?>%</h4>
										<div class="status-track">
											<div class="status-complete" style="width: <?php the_field('revestimento_externo'); ?>%"></div>
										</div>
									</div>
									<div class="status wow fadeInRight">
										<h6>fôrros</h6>
										<h4><?php the_field('forros'); ?>%</h4>
										<div class="status-track">
											<div class="status-complete" style="width: <?php the_field('forros'); ?>%"></div>
										</div>
									</div>
									<div class="status wow fadeInRight">
										<h6>paisagismo</h6>
										<h4><?php the_field('paisagismo'); ?>%</h4>
										<div class="status-track">
											<div class="status-complete" style="width: <?php the_field('paisagismo'); ?>%"></div>
										</div>
									</div>
									<div class="status wow fadeInRight">
										<h6>limpeza</h6>
										<h4><?php the_field('limpeza'); ?>%</h4>
										<div class="status-track">
											<div class="status-complete" style="width: <?php the_field('limpeza'); ?>%"></div>
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="status wow fadeInRight">
										<h6>fundação</h6>
										<h4><?php the_field('fundacao'); ?>%</h4>
										<div class="status-track">
											<div class="status-complete" style="width: <?php the_field('fundacao'); ?>%"></div>
										</div>
									</div>
									<div class="status wow fadeInRight">
										<h6>alvenaria</h6>
										<h4><?php the_field('alvenaria'); ?>%</h4>
										<div class="status-track">
											<div class="status-complete" style="width: <?php the_field('alvenaria'); ?>%"></div>
										</div>
									</div>
									<div class="status wow fadeInRight">
										<h6>revestimento interno</h6>
										<h4><?php the_field('revestimento_interno'); ?>%</h4>
										<div class="status-track">
											<div class="status-complete" style="width: <?php the_field('revestimento_interno'); ?>%"></div>
										</div>
									</div>
									<div class="status wow fadeInRight">
										<h6 class="bancadas">bancadas, louças e metais</h6>
										<h4><?php the_field('bancadas_loucas_e_metais'); ?>%</h4>
										<div class="status-track">
											<div class="status-complete" style="width: <?php the_field('bancadas_loucas_e_metais'); ?>%"></div>
										</div>
									</div>
									<div class="status wow fadeInRight">
										<h6>acabamento</h6>
										<h4><?php the_field('acabamento'); ?>%</h4>
										<div class="status-track">
											<div class="status-complete" style="width: <?php the_field('acabamento'); ?>%"></div>
										</div>
									</div>
								</div>

								<div class="col-md-12">
									<div class="status wow fadeInRight">
										<h6>Total</h6>
										<h4><?php the_field('total'); ?>%</h4>
										<div class="status-track">
											<div class="status-complete" style="width: <?php the_field('total'); ?>%"></div>
										</div>
										<p class="obras-footer"><?php the_field('rodape_12'); ?></p>
									</div>
								</div>

							</div>

						</div>
					</div>
				</div>
			</div>

			<div class="container-fluid d-md-none">
				<div class="row">
					<div class="col-md-4 localizacao">
						<h5 class="wow fadeInUp"><?php the_field('titulo_11'); ?></h5>
						<h2 class="wow fadeInUp"><?php the_field('cabecalho_11'); ?></h2>
						<div class="wow fadeInUp"><?php the_field('texto_11'); ?></div>
					</div>
				</div>
			</div>

		</section>

		<section id="mapa">
			<div id="gmaps" data-lat="<?php the_field('latitude_11'); ?>" data-lng="<?php the_field('longitude_11'); ?>"></div>
		</section>

		<section id="contato" class="white-darkblue">
			<div class="container">
				<div class="row">
					<div class="col-md-4 sobre">
						<h5 class="wow fadeInUp"><?php the_field('titulo_13'); ?></h5>
						<h2 class="wow fadeInUp"><?php the_field('cabecalho_13'); ?></h2>
						<?php the_field('texto_13'); ?>
						<a href="<?php the_field('link_13'); ?>" class="btn wow fadeIn" target="_blank"><?php the_field('botao_13'); ?></a>
					</div>
					<div class="col-md-4 offset-md-3 contato-form d-none d-md-block">
						<h5 class="wow fadeInUp"><?php the_field('titulo_14'); ?></h5>
						<h2 class="wow fadeInUp"><?php the_field('cabecalho_14'); ?></h2>
						<p class="wow fadeInUp"><?php the_field('texto_14'); ?></p>
						<?php echo do_shortcode('[contact-form-7 id="219" title="Contato"]'); ?>
					</div>
				</div>
			</div>
			<div class="container blue d-md-none">
				<div class="contato-form">
					<h5 class="wow fadeInUp"><?php the_field('titulo_14'); ?></h5>
					<h2 class="wow fadeInUp"><?php the_field('cabecalho_14'); ?></h2>
					<p class="wow fadeInUp"><?php the_field('texto_14'); ?></p>
					<?php echo do_shortcode('[contact-form-7 id="219" title="Contato"]'); ?>
				</div>
			</div>
		</section>

	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
