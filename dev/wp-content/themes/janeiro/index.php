<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordpressGulpBoilerplate
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main">

		<section id="hero" class="present">

			<div class="container hero-content">
				<h5>bem-vindo</h5>
				<h1>Expressão de viver à beira-mar no Bessa</h1>
				<p>Visite o apartamento de 241m2 sem sair de casa.</p>
				<a href="#" class="btn">FAÇA O TOUR</a>
				<div class="parallax-01 rellax animated fadeIn" data-rellax-speed="-4"><img src="<?php echo get_template_directory_uri() ?>/images/parallax-01.svg"></div>
			</div>

			<div class="hero-cta d-none d-md-block">
				<div class="parallax-02 rellax animated fadeIn" data-rellax-speed="-5"><img src="<?php echo get_template_directory_uri() ?>/images/parallax-02.svg"></div>
				<div class="container white">
					<div class="row">
						<div class="col-md-4">
							<h5 class="tag">lazer + bem-estar</h5>
							<h3>Descubra os melhores momentos da sua vida.</h3>
							<p class="sub">Saiba mais sobre a área de lazer</p>
						</div>
						<div class="col-md-4">
							<h5 class="tag">diferenciais</h5>
							<h3>Um projeto inspirado em todos os seus desejos</h3>
							<p class="sub">Conheça os diferenciais do Janeiro</p>
						</div>
						<div class="col-md-4">
							<h5 class="tag">plantas</h5>
							<h3>A mais perfeita tradução do seu estilo de vida. </h3>
							<p class="sub">Veja toda arquitetura do seu novo apê</p>
						</div>
					</div>
					<div class="parallax-03 rellax animated fadeIn" data-rellax-speed="3"><img src="<?php echo get_template_directory_uri() ?>/images/parallax-03.svg"></div>
					<div class="parallax-04 rellax animated fadeIn" data-rellax-speed="6"><img src="<?php echo get_template_directory_uri() ?>/images/parallax-04.svg"></div>
				</div>
			</div>

		</section>

		<section id="empreendimento">

			<div class="container-fluid relative">
				<div class="row">
					<div class="container">
						<div class="row">
							<div class="col-md-6 left-half">
								<img class="featured" src="<?php echo get_template_directory_uri() ?>/images/empreendimento.jpg">
								<div class="parallax-05 rellax animated fadeIn" data-rellax-speed="1"><img src="<?php echo get_template_directory_uri() ?>/images/parallax-05.svg"></div>
							</div>
							<div class="col-md-5 offset-md-7">
								<h5>o empreendimento</h5>
								<h1>Janeiro faz sentir e dá sentido. Janeiro é a poesia de viver por inteiro. </h1>
								<p>Perfeito para morar, investir ou veranear.  </p>
								<p>2 quartos à beira-mar no Bessa, com alto padrão de acabamento.</p>
								<p>Piscina e sorrisos com borda infinita. Deck para o mar. Lounge e Espaço Gourmet no térreo e na cobertura. Concierge, lavanderia, espaços e serviços compartilhados. Coworking com wi-fi.</p>
								<p>Janeiro é coletividade (com privacidade em sua essência). É inovação, tecnologia, acesso por fechadura digital. Janeiro é sustentabilidade, gentileza, etcétera e tal.</p>
							</div>
							<div class="col-md-6">
								<h1 class="uppercase">urbano <br>e praieiro. <br>janeiro é <br>urban</h1>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="parallax-container">
				<div class="container">
					<div class="owl-container">
						<div id="owlEmpreendimento" class="owl-gallery owl-carousel" data-items="1" data-padding="0">
							<div class="item">
								<img src="<?php echo get_template_directory_uri() ?>/images/slider-01.jpg">
							</div>
							<div class="item">
								<img src="<?php echo get_template_directory_uri() ?>/images/slider-01.jpg">
							</div>
							<div class="item">
								<img src="<?php echo get_template_directory_uri() ?>/images/slider-01.jpg">
							</div>
							<div class="item">
								<img src="<?php echo get_template_directory_uri() ?>/images/slider-01.jpg">
							</div>
							<div class="item">
								<img src="<?php echo get_template_directory_uri() ?>/images/slider-01.jpg">
							</div>
						</div>
						<div class="navigation-dots"></div>
						<div class="navigation-arrows"></div>
					</div>
				</div>
				<div class="parallax-06 rellax animated fadeIn" data-rellax-speed="1"><img src="<?php echo get_template_directory_uri() ?>/images/parallax-06.svg"></div>
			</div>

		</section>

		<section id="descubra">
			<div class="container">
				<div class="row">
					<div class="col">
						<h5>lazer + bem-estar</h5>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<h2>Descubra os melhores momentos da sua vida.</h2>
					</div>
					<div class="col-md-6">
						<p>Janeiro é para você e para todas as gerações. Um empreendimento que conecta as pessoas e espaços, valoriza o convívio e as interações sociais. O cenário perfeito para experimentar o tempo, sentir a vida e compartilhar momentos inesquecíveis.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="owl-container">
							<div id="owlDescubra" class="owl-gallery owl-carousel overflow" data-items="1" data-padding="100">
								<div class="item">
									<img src="<?php echo get_template_directory_uri() ?>/images/slider-02.jpg">
								</div>
								<div class="item">
									<img src="<?php echo get_template_directory_uri() ?>/images/slider-02.jpg">
								</div>
								<div class="item">
									<img src="<?php echo get_template_directory_uri() ?>/images/slider-02.jpg">
								</div>
								<div class="item">
									<img src="<?php echo get_template_directory_uri() ?>/images/slider-02.jpg">
								</div>
							</div>
							<div class="navigation-dots"></div>
							<div class="navigation-arrows"></div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section id="diferenciais">
			<div class="container">
				<div class="parallax-07 rellax animated fadeIn" data-rellax-speed="1"><img src="<?php echo get_template_directory_uri() ?>/images/parallax-07.svg"></div>
				<div class="row">
					<div class="col-md-12">
						<h5>diferenciais</h5>
						<h2>Um projeto inspirado em todos os seus desejos</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="owl-container">
							<div id="owlDiferenciais" class="owl-gallery owl-carousel overflow" data-items="2">
								<div class="item">
									<img src="<?php echo get_template_directory_uri() ?>/images/slider-03.jpg">
								</div>
								<div class="item">
									<img src="<?php echo get_template_directory_uri() ?>/images/slider-03.jpg">
								</div>
								<div class="item">
									<img src="<?php echo get_template_directory_uri() ?>/images/slider-03.jpg">
								</div>
								<div class="item">
									<img src="<?php echo get_template_directory_uri() ?>/images/slider-03.jpg">
								</div>
							</div>
							<div class="navigation-dots"></div>
							<div class="navigation-arrows"></div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section id="projeto">
			<a href="https://www.youtube.com/watch?v=rWzMreEelHQ&ab_channel=LeandroS%C3%A1" data-fancybox class="btn-play"></a>
			<div class="container-fluid half-white">
				<div class="container">
					<div class="row">
						<div class="col-md-7 white header">
							<h5>o projeto</h5>
							<h2>A arquitetura contemporânea e suas formas de expressão</h2>
						</div>
					</div>
				</div>
			</div>
			<div class="container-fluid white">
				<div class="container">
					<div class="row">
						<div class="col-md-7">
							<p>Janeiro tem a assinatura da Urban Incorporadora e projeto do arquiteto paraibano Gilberto Guedes, um dos mais aclamados profissionais da arquitetura do Nordeste, reconhecido pela atenção aos detalhes, rigor técnico e integração de seus projetos com o entorno.</p>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section id="plantas">
			<div class="parallax-08 rellax animated fadeIn" data-rellax-speed="2"><img src="<?php echo get_template_directory_uri() ?>/images/parallax-08.svg"></div>
			<div class="container-fluid relative">
				<div class="row">
					<div class="container">
						<div class="row">
							<div class="col-md-6">
								<div class="plantas-details">
									<h5>plantas</h5>
									<h2>A mais perfeita tradução <br>do seu estilo de vida. </h2>
									<h6>apartamentos</h6>
									<ul>						
										<li>- 92m2 a 241m2</li>
										<li>- 2 e 3 quartos</li>
										<li>- 2 ou 3 vagas de garagem</li>
									</ul>
									<h6>áreas comuns</h6>
									<ul>
										<li>- Piscina com borda infinita</li>
										<li>- Salão de festas</li>
										<li>- Academia</li>
										<li>- Brinquedoteca</li>
										<li>- Pátio Central</li>
										<li>- Restaurante Gourmet & Coworking</li>
										<li>- Bicicletário</li>
									</ul>
									<h6>Previsão de entrega</h6>
									<ul>
										<li>23/09/2023</li>
									</ul>
								</div>
							</div>
							<div class="col-md-6 right-half">
								<div class="owl-container half-white-v">
									<div id="owlPlantas" class="owl-gallery owl-carousel" data-items="1">
										<div class="item">
											<div class="item-img">
												<img src="<?php echo get_template_directory_uri() ?>/images/planta.png">
											</div>
											<div class="item-sub">
												<h3>coberturas</h3>
												<p>Lounge gourmet, piscina com borda inifinita e deck.</p>
											</div>
										</div>
										<div class="item">
											<div class="item-img">
												<img src="<?php echo get_template_directory_uri() ?>/images/planta.png">
											</div>
											<div class="item-sub">
												<h3>coberturas</h3>
												<p>Lounge gourmet, piscina com borda inifinita e deck.</p>
											</div>
										</div>
										<div class="item">
											<div class="item-img">
												<img src="<?php echo get_template_directory_uri() ?>/images/planta.png">
											</div>
											<div class="item-sub">
												<h3>coberturas</h3>
												<p>Lounge gourmet, piscina com borda inifinita e deck.</p>
											</div>
										</div>
										<div class="item">
											<div class="item-img">
												<img src="<?php echo get_template_directory_uri() ?>/images/planta.png">
											</div>
											<div class="item-sub">
												<h3>coberturas</h3>
												<p>Lounge gourmet, piscina com borda inifinita e deck.</p>
											</div>
										</div>
										<div class="item">
											<div class="item-img">
												<img src="<?php echo get_template_directory_uri() ?>/images/planta.png">
											</div>
											<div class="item-sub">
												<h3>coberturas</h3>
												<p>Lounge gourmet, piscina com borda inifinita e deck.</p>
											</div>
										</div>
									</div>
									<div class="navigation-dots"></div>
									<div class="navigation-arrows"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section id="ebook">
			<div class="ebook-padding">
				<div class="parallax-09 rellax animated fadeIn" data-rellax-speed="1"><img src="<?php echo get_template_directory_uri() ?>/images/parallax-09.svg"></div>
				<div class="parallax-10 rellax animated fadeIn" data-rellax-speed="1"><img src="<?php echo get_template_directory_uri() ?>/images/parallax-10.svg"></div>
			</div>
			<div class="container-fuild blue-purple">
				<div class="row">
					<div class="container">
						<div class="row">
							<div class="col-md-5 ebook-cover">
								<img src="<?php echo get_template_directory_uri() ?>/images/ebook.svg">
							</div>
							<div class="col-md-7 text-center purple">
								<h5>E-book</h5>
								<h2>Baixe nosso ebook e tenha um conteúdo exclusivo do Janeiro</h2>
								<a href="#" class="btn secondary">BAIXAR EBOOK</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section id="obra">
			<div class="container-fluid white-pink">
				<div class="container">
					<div class="row">
						<div class="col-md-4 localizacao">
							<h5>localização</h5>
							<h2>Praia, tranquilidade e diversão.</h2>
							<p>Em meio a tantas belezas naturais,  um bairro nobre e encantador, com orla urbanizada, restaurantes e bares sofisticados, escolas, supermercados e toda a estrutura para você viver com mais conforto e praticidade. Não é à toa que Bessa é uma das praias urbanas preferidas por moradores e turistas (e também pelas tartarugas). Um lugar incrível para apreciar a natureza, compartilhar o tempo e viver momentos especiais.</p>
						</div>
						<div class="col-md-8 andamento pink">
							<h2>Andamento da obra</h2>
							<p>Ínicio do projeto: <strong>25/08/2020</strong></p>

							<div class="row status-list">

								<div class="col-md-6">
									<div class="status">
										<h6>projeto</h6>
										<h4>100%</h4>
										<div class="status-track">
											<div class="status-complete" style="width: 100%"></div>
										</div>
									</div>
									<div class="status">
										<h6>estrutura</h6>
										<h4>63%</h4>
										<div class="status-track">
											<div class="status-complete" style="width: 63%"></div>
										</div>
									</div>
									<div class="status">
										<h6>revestimento externo</h6>
										<h4>100%</h4>
										<div class="status-track">
											<div class="status-complete" style="width: 100%"></div>
										</div>
									</div>
									<div class="status">
										<h6>fôrros</h6>
										<h4>100%</h4>
										<div class="status-track">
											<div class="status-complete" style="width: 100%"></div>
										</div>
									</div>
									<div class="status">
										<h6>paisagismo</h6>
										<h4>100%</h4>
										<div class="status-track">
											<div class="status-complete" style="width: 100%"></div>
										</div>
									</div>
									<div class="status">
										<h6>limpeza</h6>
										<h4>100%</h4>
										<div class="status-track">
											<div class="status-complete" style="width: 100%"></div>
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="status">
										<h6>fundação</h6>
										<h4>100%</h4>
										<div class="status-track">
											<div class="status-complete" style="width: 100%"></div>
										</div>
									</div>
									<div class="status">
										<h6>alvenaria</h6>
										<h4>100%</h4>
										<div class="status-track">
											<div class="status-complete" style="width: 100%"></div>
										</div>
									</div>
									<div class="status">
										<h6>revestimento interno</h6>
										<h4>100%</h4>
										<div class="status-track">
											<div class="status-complete" style="width: 100%"></div>
										</div>
									</div>
									<div class="status">
										<h6 class="bancadas">bancadas, louças e metais</h6>
										<h4>100%</h4>
										<div class="status-track">
											<div class="status-complete" style="width: 100%"></div>
										</div>
									</div>
									<div class="status">
										<h6>acabamento</h6>
										<h4>100%</h4>
										<div class="status-track">
											<div class="status-complete" style="width: 100%"></div>
										</div>
									</div>
								</div>

								<div class="col-md-12">
									<div class="status">
										<h6>Total</h6>
										<h4>46%</h4>
										<div class="status-track">
											<div class="status-complete" style="width: 46%"></div>
										</div>
										<p>O total do estágio da obra é calculado por média ponderada</p>
									</div>
								</div>
								
							</div>

						</div>
					</div>
				</div>
			</div>
		</section>

		<section id="mapa">
			<div id="gmaps" data-lat="-7.0674147" data-lng="-34.8406027"></div>
		</section>

		<section id="contato" class="white-darkblue">
			<div class="container">
				<div class="row">
					<div class="col-md-4 sobre">
						<h5>a urban</h5>
						<h2>Ser, <br>estar e <br>fazer</h2>
						<p>A Urban entende que cada pessoa possui sua individualidade e ao mesmo tempo vive coletivamente. Nós acreditamos que podemos proporcionar os melhores ambientes para que você viva experiências de vida completas, integrando a sua individualidade com a comunidade em que você está.</p>
						<a href="#" class="btn">SAIBA MAIS</a>
					</div>
					<div class="col-md-4 offset-md-3 contato-form">
						<h5>contato</h5>
						<h2>Tem interesse?</h2>
						<p>Preencha o formulário e entraremos em contato com você.</p>
						<form action="#">
							<input type="text" placeholder="Nome">
							<input type="text" placeholder="Nome">
							<input type="text" placeholder="Nome">
							<input type="text" placeholder="Nome">
							<input type="submit" value="Enviar" class="btn disabled" disabled>
						</form>
					</div>
				</div>
			</div>
		</section>

	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
