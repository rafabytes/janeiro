<!DOCTYPE html>
<html>
<head>
<title>Project Title</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, minimal-ui" />
<style> @-ms-viewport { width: device-width; } </style>
<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/template-parts/tour/vendor/reset.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/template-parts/tour/style.css">
</head>
<body class="multiple-scenes ">

<div id="pano"></div>

<div id="sceneList">
  <ul class="scenes">
    
      <a href="javascript:void(0)" class="scene" data-id="0-trreo">
        <li class="text">Térreo</li>
      </a>
    
      <a href="javascript:void(0)" class="scene" data-id="1-superior">
        <li class="text">Superior</li>
      </a>
    
  </ul>
</div>

<div id="titleBar">
  <h1 class="sceneName"></h1>
</div>

<a href="javascript:void(0)" id="autorotateToggle">
  <img class="icon off" src="<?php echo get_template_directory_uri() ?>/template-parts/tour/img/play.png">
  <img class="icon on" src="<?php echo get_template_directory_uri() ?>/template-parts/tour/img/pause.png">
</a>

<a href="javascript:void(0)" id="fullscreenToggle">
  <img class="icon off" src="<?php echo get_template_directory_uri() ?>/template-parts/tour/img/fullscreen.png">
  <img class="icon on" src="<?php echo get_template_directory_uri() ?>/template-parts/tour/img/windowed.png">
</a>

<a href="javascript:void(0)" id="sceneListToggle">
  <img class="icon off" src="<?php echo get_template_directory_uri() ?>/template-parts/tour/img/expand.png">
  <img class="icon on" src="<?php echo get_template_directory_uri() ?>/template-parts/tour/img/collapse.png">
</a>

<a href="javascript:void(0)" id="viewUp" class="viewControlButton viewControlButton-1">
  <img class="icon" src="<?php echo get_template_directory_uri() ?>/template-parts/tour/img/up.png">
</a>
<a href="javascript:void(0)" id="viewDown" class="viewControlButton viewControlButton-2">
  <img class="icon" src="<?php echo get_template_directory_uri() ?>/template-parts/tour/img/down.png">
</a>
<a href="javascript:void(0)" id="viewLeft" class="viewControlButton viewControlButton-3">
  <img class="icon" src="<?php echo get_template_directory_uri() ?>/template-parts/tour/img/left.png">
</a>
<a href="javascript:void(0)" id="viewRight" class="viewControlButton viewControlButton-4">
  <img class="icon" src="<?php echo get_template_directory_uri() ?>/template-parts/tour/img/right.png">
</a>
<a href="javascript:void(0)" id="viewIn" class="viewControlButton viewControlButton-5">
  <img class="icon" src="<?php echo get_template_directory_uri() ?>/template-parts/tour/img/plus.png">
</a>
<a href="javascript:void(0)" id="viewOut" class="viewControlButton viewControlButton-6">
  <img class="icon" src="<?php echo get_template_directory_uri() ?>/template-parts/tour/img/minus.png">
</a>

<script src="<?php echo get_template_directory_uri() ?>/template-parts/tour/vendor/es5-shim.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/template-parts/tour/vendor/eventShim.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/template-parts/tour/vendor/classList.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/template-parts/tour/vendor/requestAnimationFrame.js" ></script>
<script src="<?php echo get_template_directory_uri() ?>/template-parts/tour/vendor/screenfull.min.js" ></script>
<script src="<?php echo get_template_directory_uri() ?>/template-parts/tour/vendor/bowser.min.js" ></script>
<script src="<?php echo get_template_directory_uri() ?>/template-parts/tour/vendor/marzipano.js" ></script>

<script src="<?php echo get_template_directory_uri() ?>/template-parts/tour/data.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/template-parts/tour/index.js"></script>

</body>
</html>
