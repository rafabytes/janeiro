<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_janeiro');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0D}l*36b/lSzk!3z8r1Cv$r-8|Yw9xpdb0[s6ip;B?]J3Cu+`xEYhqLZ8Abzt_0>');
define('SECURE_AUTH_KEY',  'I<J|x_75{k{t:c@n`3e1K#@D^ K~IU6*Q&WD77>SWvt:v4vqwB7%~13g$AokEsX/');
define('LOGGED_IN_KEY',    'Q*r!<)t1n5zu[}HY$NgM3~>}.Pxc+.+y.Cf5W2^Z=8>B$*P2WJ[euzvyT:cm.lD4');
define('NONCE_KEY',        'H$(@vFg7+nwp}07?wG0h)wMh-9G2|?(AKtjD!n!1LZS9*7*p>L>N7g;4qX!vR-oc');
define('AUTH_SALT',        '@y1eMWr{:iY=Dq=f=}~<W8IY7c~L:{gKJ4:C96[e];h3zKS8w#eklJ4|C,AeAHz!');
define('SECURE_AUTH_SALT', '%5mQE{dDb>hwZ5(waImnu=fRyxv(HH]*n<)p4d]=<E9yr=t+4-Tv6w%^F;CS8iN4');
define('LOGGED_IN_SALT',   'ALNgn>KBSNeWFSDE{6Hb ]+WvAvAv-B?>HG(1*m:g(t~%iN>NXQ[8~CdSTblNyZ|');
define('NONCE_SALT',       '1<3O&(gt?u.S6X7F0|]z?~6G>1_L`up;2{O[O+n{0RlpOJIxFCL:_ya|Lf<fxmJ-');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
